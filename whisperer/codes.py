undefined = {"code": "undefined", "en": "undefined exception code"}

webhook_100_1 = {"code": "webhook_100_1", "en": "Webhook does not exist"}

webhook_100_2 = {"code": "webhook_100_2", "en": "Webhook already registered"}

event_100_1 = {"code": "event_100_1", "en": "WebhookEvent does not exist"}

event_100_2 = {"code": "event_100_2", "en": "Unknown event type"}

event_100_3 = {"code": "event_100_3", "en": "Event already delivered"}

event_100_4 = {
    "code": "event_100_4",
    "en": "Events must be inherited from WhispererEvent",
}

event_100_5 = {"code": "event_100_5", "en": "{event_type} is already registered"}

event_100_6 = {"code": "event_100_6", "en": "Event is still in retry process"}
